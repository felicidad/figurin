# Figurin

Hapi models using Objection / Knex. This plugin enables you to access your Objection models from anywhere in the hapi ecosystem. It is designed to be simple, light, and elegant.

##### Peer Dependencies

This package works with the following:

```
"hoek": "^6.x.x",
"knex": "^0.16.x",
"joi": ">=14.x.x",
"objection": ">=1.6.x",
"wranch": ">=1.0.3"
```
## Usage

``` javascript


await server.register({
    plugin: require('figurin'),
    options: {

        // Specify the models path for import
        modelsPath: './db/models',

        // Migrate on start
        migrateOnStart: false,

        // Seed, rollback, and helper routes
        // Only work when not in production
        seedOnStart: false,
        rollbackOnStop: false,
        helperRoutes: false,

        // Specify your DB config with explicit
        // specification to migration and seed directories
        dbConfig: {
            client: 'sqlite3',
            useNullAsDefault: true,
            connection: {
                filename: './db/db.sqlite'
            },
            migrations: {
                directory: './db/migrations'
            },
            seeds: {
                directory: './db/seeds'
            }
        }
    }
});

// server.models() gives you access to the models
const {
    User,
    Post,
    Comment
} = server.models();

// You can return only 1 model if you'd like
const Order = server.models('Order');

// server.db() gives you access to knex
// and knex wrapper functions
const {
    migrate,
    rollback,
    knex
} = server.db();

// You can request only one of the functions
const seed = server.db('seed');

```

---

### API

##### models()

Gives you access to your models as an object. If you pass an argument, you can retrieve only 1 specific model, for example: `models('User')`. `figurin` gives access to this functionality in `server`, `request`, and `h`:

```javascript
const { User } = server.models()
```

```javascript
server.route({
    method: 'GET',
    path: '/users',
    handler: (request, h) => {

        const { User } = request.models();
        const { Posts } = h.models();
    }
})

```

This redundancy is useful in cases where you may want to declare your routes in separate files and folders, or create decorators. You can access the same functions using this shorthand method.


##### db()

Gives you access to knex functionality as an object. This method also allows an argument, for example: `db('knex')`. Similarly to `models()`, this functionality is accessible in `server`, `request`, and `h`:

```javascript
const { knex } = server.db()
```

```javascript
server.route({
    method: 'GET',
    path: '/resetdb',
    handler: (request, h) => {

        const { rollback } = request.db();
        const { migrate } = h.db();
    }
});
```

The avaible keys are as follows:
- `knex` - The configured knex object
- `migrate` - A wrapper for `knex.schema.migrate`
- `seed` - A wrapper for `knex.schema.seed`
- `rollback` - A wrapper for `knex.schema.rollback`



### Options


##### modelsPath - String


Models path automatically requires all the files inside of the specified directory. It registers the model as whatever your filename. For example, if a file is named `users.js`, the model would be accessed as `server.models('users')`; if the file is name `User.js`, the model would accessed as `server.models('User')`.


##### migrateOnStart - Boolean

Runs migrations on start. This functionality works in all environments.

##### seedOnStart - Boolean

Runs seeds on start. This functionality does not work in `production` environment to not accidentally seed a production database.

##### rollbackOnStop - Boolean

Rolls back migrations on server stop. This functionality does not work in `production` environment to not accidentally rollback a production database.

##### helperRoutes - Boolean

Gives the ability to migrate, seed and rollback the DB using URLs. Respectively, they are:
- `/knex/migrate`
- `/knex/seed`
- `/knex/rollback`

This functionality does not work in `production` environment for security reasons.

##### dbConfig - Object

Specifies the DB config passed into `knex`. In order for `migrate`, `seed`, and `rollback` to work properly, you must specify their directories, [as shown here](#usage).

# Contributing

Feel free to contribute to this project. Contributions must include tests and documentation, if necessary.