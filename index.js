const Fs = require('fs');
const Joi = require('joi');
const Hoek = require('hoek');
const Knex = require('knex');
const { Model } = require('objection');
const { requireFiles } = require('wranch');

const optionsSchema = Joi.object().keys({

    modelsPath: Joi.string().required(),
    migrateOnStart: Joi.boolean().optional(),
    seedOnStart: Joi.boolean().optional(),
    rollbackOnStop: Joi.boolean().optional(),
    helperRoutes: Joi.boolean().optional(),
    dbConfig: Joi.object().required()
});

module.exports = {

    name: 'figurin',
    register: function (server, options) {

        const dev = process.env.NODE_ENV !== 'production';

        Hoek.assert(options);
        const { error } = optionsSchema.validate(options);

        Hoek.assert(!error, error);

        const {
            modelsPath,
            migrateOnStart,
            seedOnStart,
            rollbackOnStop,
            helperRoutes,
            dbConfig
        } = options;

        Hoek.assert(Fs.statSync(modelsPath).isDirectory(), `${modelsPath} is not a directory`);
        Hoek.assert(Fs.readdirSync(modelsPath).length, `${modelsPath} is empty`);
        const knex = Knex(dbConfig);
        Model.knex(knex);

        const models = requireFiles(modelsPath);

        const store = {
            migrate: () => knex.migrate.latest(),
            rollback: () => knex.migrate.rollback(),
            seed: () => knex.seed.run(),
            knex
        };

        const getDbStore = function (key = false) {

            return key ? store[key] : store;
        };

        const getModels = function (key = false) {

            return key ? models[key] : models;
        };

        server.decorate('server', 'db', getDbStore);
        server.decorate('request', 'db', getDbStore);
        server.decorate('toolkit', 'db', getDbStore);

        server.decorate('server', 'models', getModels);
        server.decorate('request', 'models', getModels);
        server.decorate('toolkit', 'models', getModels);


        if (migrateOnStart) {
            server.ext('onPreStart', store.migrate);
        }

        if (dev && seedOnStart) {
            server.ext('onPreStart', store.seed);
        }

        if (dev && rollbackOnStop) {
            server.ext('onPreStop', store.rollback);
        }

        if (dev && helperRoutes) {
            server.route({ method: 'GET', path: `/knex/migrate`, handler: store.migrate });
            server.route({ method: 'GET', path: `/knex/rollback`, handler: store.rollback });
            server.route({ method: 'GET', path: `/knex/seed`, handler: store.seed });
        }

    }
};
