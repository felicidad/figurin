// NPM Modules
const Path = require('path');
const Fs = require('fs');
const Lab  = require('lab');
const Code = require('code');
const Hapi = require('hapi');

// App Modules
const Figurin = require('..');

// Module Variables
const {
    experiment,
    it,
    before,
    after,
    beforeEach,
    afterEach
} = exports.lab = Lab.script();
const expect = Code.expect;

const stub = {

    options: {
        modelsPath: Path.resolve(__dirname, 'models'),
        migrateOnStart: false,
        rollbackOnStop: false,
        helperRoutes: false,
        dbConfig: {

            client: 'sqlite3',
            useNullAsDefault: true,
            connection: {
                filename: Path.resolve(__dirname, 'db.sqlite')
            },
            migrations: {
                directory: Path.resolve(__dirname, 'migrations')
            },
            seeds: {
                directory: Path.resolve(__dirname, 'seeds')
            }
        }
    }
};

experiment('Figurin',  () => {

    experiment('API',  () => {

        before(async () => {

            stub.server = Hapi.Server({ port: 4321 });
            await stub.server.register({
                plugin: Figurin,
                options: stub.options
            });
        });

        after(() => {

            Fs.unlinkSync(Path.resolve(__dirname, 'db.sqlite'));
        });

        it('creates decorators named namespace option', () => {

            expect(stub.server.decorations.request).to.include(['db', 'models']);
            expect(stub.server.decorations.server).to.include(['db', 'models']);
            expect(stub.server.decorations.toolkit).to.include(['db', 'models']);
        });

        it('exposes knex, seed, and migration functionality', () => {

            const fns = stub.server.db();

            expect(
                Object.keys(
                    fns
                )
            )
                .to.include(['migrate', 'rollback', 'knex', 'seed']);

            expect(fns.knex).to.be.an.function();
            expect(fns.migrate).to.be.a.function();
            expect(fns.rollback).to.be.a.function();
            expect(fns.seed).to.be.a.function();
        });

        it('exposes a key from db function', () => {

            const fns = stub.server.db();
            const knex = stub.server.db('knex');

            expect(fns.knex).to.shallow.equal(knex);
        });

        it('exposes models', () => {

            const models = stub.server.models();

            expect(models).to.include(['Post', 'User']);
        });

        it('exposes a key from models', () => {

            const models = stub.server.models();
            const User = stub.server.models('User');

            expect(models.User).to.shallow.equal(User);
        });

        it('migrates a database', async () => {

            const { migrate } = stub.server.db();

            const [vers, files] = await migrate();

            expect(vers).to.equal(1);
            expect(files).to.include(['01_users.js', '02_posts.js']);
        });

        it('queries the models', async () => {

            const { User } = stub.server.models();
            expect(await User.query()).to.be.empty();
        });

        it('seeds the database', async () => {

            const { seed } = stub.server.db();
            const { User } = stub.server.models();
            expect((await seed())[0].length).to.equal(2);
            expect((await User.query()).length).to.above(2);
        });

        it('seeds the database', async () => {

            const { seed } = stub.server.db();
            expect((await seed())[0].length).to.equal(2);
        });

        it('rolls back the database', async () => {

            const { seed, rollback } = stub.server.db();
            const [vers, files] = await rollback();

            expect(vers).to.equal(1);
            expect(files).to.include(['01_users.js', '02_posts.js']);

            try {
                expect(await seed()).to.not.be.an.array();
            }
            catch (e) {

                expect(e).to.exist();
            }
        });
    });

    experiment('Options and Events',  () => {

        before(() => {

            Object.assign(stub.options, {
                migrateOnStart: true,
                rollbackOnStop: true,
                seedOnStart: true,
                helperRoutes: true
            });
        });

        after(() => {

            Fs.unlinkSync(Path.resolve(__dirname, 'db.sqlite'));
        });

        beforeEach(async () => {

            stub.server = Hapi.Server({ port: 54321 });
            await stub.server.register({
                plugin: Figurin,
                options: stub.options
            });

            await stub.server.start();
        });

        afterEach(async () => {

            await stub.server.stop();
        });

        it('migrates the database on start', async () => {

            const { knex } = stub.server.db();

            const hasUsers = await knex.schema.hasTable('users');
            const hasPosts = await knex.schema.hasTable('posts');

            expect(hasUsers).to.equal(true);
            expect(hasPosts).to.equal(true);
        });

        it('seeds the database on start', async () => {

            const { User } = stub.server.models();

            expect((await User.query()).length).to.equal(3);
        });

        it('rolls back the database on stop', async () => {

            await stub.server.stop();

            const { knex } = stub.server.db();

            const hasUsers = await knex.schema.hasTable('users');
            const hasPosts = await knex.schema.hasTable('posts');

            expect(hasUsers).to.equal(false);
            expect(hasPosts).to.equal(false);

            // Prep next test
            process.env.NODE_ENV = 'production';
        });

        it('does not seed the database if in production', async () => {

            const { knex } = stub.server.db();

            const hasUsers = await knex.schema.hasTable('users');
            const hasPosts = await knex.schema.hasTable('posts');

            expect(hasUsers).to.equal(true);
            expect(hasPosts).to.equal(true);

            const { User } = stub.server.models();
            expect((await User.query()).length).to.equal(0);
        });

        it('does not roll back the database in production', async () => {

            await stub.server.stop();

            const { knex, rollback } = stub.server.db();

            const hasUsers = await knex.schema.hasTable('users');
            const hasPosts = await knex.schema.hasTable('posts');

            expect(hasUsers).to.equal(true);
            expect(hasPosts).to.equal(true);

            // Prep next test
            await rollback();
            process.env.NODE_ENV = 'test';
            Object.assign(stub.options, {
                migrateOnStart: false,
                rollbackOnStop: false,
                seedOnStart: false
            });
        });

        it('has helper routes', () => {

            const routes = stub.server.table().map(({ path }) => path);

            expect(routes).to.include([
                '/knex/migrate',
                '/knex/rollback',
                '/knex/seed'
            ]);
        });

        it('migrates using helper route', async () => {

            const { knex, rollback } = stub.server.db();

            await rollback();

            const { result } = await stub.server.inject('/knex/migrate');

            expect(result).to.contain([1, ['01_users.js', '02_posts.js']]);


            const hasUsers = await knex.schema.hasTable('users');
            const hasPosts = await knex.schema.hasTable('posts');

            expect(hasUsers).to.equal(true);
            expect(hasPosts).to.equal(true);

            const { User } = stub.server.models();

            expect((await User.query()).length).to.equal(0);

        });

        it('seeds using helper route', async () => {

            const { result } = await stub.server.inject('/knex/seed');

            expect(result[0].length).to.equal(2);

            const { User } = stub.server.models();

            expect((await User.query()).length).to.equal(3);
        });

        it('rolls back using helper route', async () => {

            const { result } = await stub.server.inject('/knex/rollback');

            expect(result).to.contain([1, ['02_posts.js', '01_users.js']]);

            const { knex } = stub.server.db();

            const hasUsers = await knex.schema.hasTable('users');
            const hasPosts = await knex.schema.hasTable('posts');

            expect(hasUsers).to.equal(false);
            expect(hasPosts).to.equal(false);
        });

    });
});
