exports.seed = function (knex, Promise) {
    // Deletes ALL existing entries
    return knex('users').del().then(() =>
        // Inserts seed entries
        knex('users').insert([
            {
                id: 1,
                firstName: 'pepito',
                lastName: 'pepillo'
            },
            {
                id: 2,
                firstName: 'fulanito',
                lastName: 'fulero'
            },
            {
                id: 3,
                firstName: 'martino',
                lastName: 'matraca'
            }
        ])
    );
};
