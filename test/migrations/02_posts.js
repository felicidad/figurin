exports.up = function (knex, Promise) {

    return Promise.all([
        knex.schema.createTable('posts', (table) => {

            table.increments('id').primary();
            table.string('title').notNullable();
            table.string('body').notNullable();
            table.integer('user_id')
                .unsigned()
                .references('users.id');

        })
    ]);
};

exports.down = function (knex, Promise) {

    return Promise.all([knex.schema.dropTable('posts')]);
};
